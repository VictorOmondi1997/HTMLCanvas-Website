let canvas = document.querySelector("canvas"),
    context = canvas.getContext('2d'),
    no = window.prompt("Enter your favourite number", "250"),
    a = 0,
    color = 360 * Math.random();

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;


if(no==""||no==undefined|| no==null){
    no=250;
}


function animate() {
    a+=0.01;
    context.beginPath();
    let x = canvas.width / 2 + canvas.width / 2 * Math.cos(no * a) * Math.cos(a);
    let y = -(-canvas.height / 2 + canvas.width / 2 * Math.cos(no * a) * Math.sin(a));
    context.arc(x,y,1,0,2*Math.PI,false);
    context.strokeStyle ="hsl("+color++ +",100%,50%)";
    context.stroke();
    if(a > 6.5){
        a = 0.01;
        no = Math.floor (Math.random () * 9) + 2;
        context.clearRect (0 , 0 , canvas.width , canvas.height);
    }
    requestAnimationFrame(animate)|| webkitRequestAnimationFrame(animate)|| mozRequestAnimationFrame(animate) || oRequestAnimationFrame (animate)|| msRequestAnimationFrame(animate);
}
requestAnimationFrame(animate)|| webkitRequestAnimationFrame(animate)|| mozRequestAnimationFrame(animate) || oRequestAnimationFrame (animate)|| msRequestAnimationFrame(animate);